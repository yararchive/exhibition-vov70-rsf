$(document).ready(function() {

  $(window).load(function() {
    $("#content-loader").delay(1000).fadeOut(1000);
  });

  $(".part-link.part0").click(function() {
    $("#part-menu").slideUp(500);
    $("#part1, #part2, #part3, #part4").delay(800).fadeOut(1000);
    $("#part0").delay(800).fadeIn(1000);
  });

  $(".part-link.part1").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part2, #part3, #part4").delay(800).fadeOut(1000);
    $("#part1").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".part-link.part2").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part1, #part3, #part4").delay(800).fadeOut(1000);
    $("#part2").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".part-link.part3").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part1, #part2, #part4").delay(800).fadeOut(1000);
    $("#part3").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".part-link.part4").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part1, #part2, #part3").delay(800).fadeOut(1000);
    $("#part4").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".fancybox").fancybox({
    closeBtn  : false,
    arrows    : true,
    nextClick : false,
    mouseWheel: true,

    tpl: {
         error: '<p class="fancybox-error">Невозможно загрузить изображение.<br/>Повторите попытку позже.<br/>Если ошибка будет повторяться,<br/>вы можете написать на <a href="mailto:zvorygin@yararchive.ru?subject=Проблемы с показом изображений в электронной выставке Ростов в годы Великой Отечественной войны.">zvorygin@yararchive.ru</a></p>'
    },

    helpers : {
      thumbs : {
        width  : 50,
        height : 50
      }
    }
  });
});